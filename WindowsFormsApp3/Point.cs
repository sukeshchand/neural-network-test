﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }
        public int Label { get; set; }

        public Point(int height, int width)
        {
            var rand = new Random(Guid.NewGuid().GetHashCode());
            X = rand.Next(20, width + 130);
            Y = rand.Next(20, height + 130);
            Label = X > Y ? 1 : -1;
        }

        public void Show(Graphics graphics)
        {
            CommonFunctions.DrawCircle(X, Y, 40, 40, graphics, Label == 1 ? Color.DarkOrange : Color.DarkOrange);
        }
    }
}
