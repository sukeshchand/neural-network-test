﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class Perceptron
    {
        public float LearningRate { get; set; }
        public Perceptron()
        {
            LearningRate = (float)0.0001;
            WeightX = 0;
            WeightY = 0;
        }

        //activation function
        private int Sign(float n)
        {
            return n >= 0 ? 1 : -1;
        }

        public int Guess(float inputX, float inputY)
        {
            float sum = 0;
            sum += inputX * WeightX;
            sum += inputY * WeightY;

            return Sign(sum);
        }

        public void Train(float inputX, float inputY, int target)
        {
            int guess = Guess(inputX, inputY);
            int error = target - guess;

            // tune all the weights
            WeightX = WeightX + error * inputX * LearningRate;
            WeightY = WeightY + error * inputY * LearningRate;
        }
        public float WeightX { get; set; }
        public float WeightY { get; set; }
    }
}
