﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public static class CommonFunctions
    {
        public static void DrawCircle(float x, float y, float width, float height, Graphics graphics, Color color)
        {
            var myPen = new Pen(color, 2);
            graphics.DrawEllipse(myPen, x, y, width, height);
        }

        public static void FillEllipse(float left, float right, float width, float height, Brush color, Graphics g)
        {
            g.FillEllipse(color, new Rectangle((int)left, (int)right, (int)width, (int)height));
        }
        public static void DrawLine(float x1, float y1, float x2, float y2, Graphics graphics, Color color)
        {
            var myPen = new Pen(color, 2);
            graphics.DrawLine(myPen, x1, y1, x2, y2);
        }
    }
}
