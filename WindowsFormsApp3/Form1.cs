﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        private int totalIndex = 0;
        public Point[] points = null;
        Perceptron brain = new Perceptron();
        public Form1()
        {
            InitializeComponent();
            txtLearningRate.Text = "0.01";
            brain.LearningRate = ToFloat(txtLearningRate.Text);
            txtTotalPoints.Text = "100";
            points = new Point[(int)ToFloat(txtTotalPoints.Text)];
        }

        public float ToFloat(string s)
        {
            return float.TryParse(s, out float f) ? f : 0;
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            points = new Point[(int)ToFloat(txtTotalPoints.Text)];
            brain.LearningRate = ToFloat(txtLearningRate.Text);
            Graphics g = base.CreateGraphics();
            g.Clear(Color.White);
            InitSamples(g);
            CommonFunctions.DrawLine(0, 0, 900, 900, g, Color.Black);

        }

        private void InitSamples(Graphics g)
        {
            //points[0] = new Point(10, 40);
            //points[1] = new Point(40, 10);
            //points[2] = new Point(100, 200);
            //points[3] = new Point(200, 100);
            //points[0].Show(g);
            //points[1].Show(g);
            //points[2].Show(g);
            //points[3].Show(g);



            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new Point(500, 500);
                points[i].Show(g);
                System.Threading.Thread.Sleep(1);
                Application.DoEvents();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnTrain_Click(object sender, EventArgs e)
        {
            //if(totalIndex> points.Length) { return; }
            //totalIndex++;
            brain.LearningRate = ToFloat(txtLearningRate.Text);
            Graphics g = base.CreateGraphics();
            int pointIndex = 0;
            foreach (Point pt in points)
            {
                //var pt = points[totalIndex];
                pointIndex++;
                int index = 0;
                while (true)
                {
                    index++;
                    label1.Text = $"{pointIndex} : Points ({pt.X},{pt.Y}) - Train index: {index} - WeightX:{brain.WeightX}, WeightY:{brain.WeightY}";
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(100);
                    var trainF = TrainOnePoint(pt);
                    if (trainF == pt.Label)
                    {
                        if (pt.Label == 1)
                        {
                            CommonFunctions.FillEllipse(pt.X + 18, pt.Y + 18, 10, 10, Brushes.DeepPink, g);
                        }
                        else
                        {
                            CommonFunctions.FillEllipse(pt.X + 18, pt.Y + 18, 10, 10, Brushes.BlueViolet, g);
                        }
                        CommonFunctions.FillEllipse(pt.X + 8, pt.Y + 8, 10, 10, Brushes.Green, g);
                        break;
                    }
                    else
                    {
                        CommonFunctions.FillEllipse(pt.X + 8, pt.Y + 8, 10, 10, Brushes.Red, g);
                    }
                }
            }
            //foreach (Point pt in points)
            //{

            //}
        }

        private int TrainOnePoint(Point pt)
        {
            brain.Train(pt.X, pt.Y, pt.Label);
            var guess = brain.Guess(pt.X, pt.Y);
            return guess;
        }

        private void btnThink_Click(object sender, EventArgs e)
        {
            brain.LearningRate = ToFloat(txtLearningRate.Text);
            Graphics g = base.CreateGraphics();
            foreach (Point pt in points)
            {
                var guess = brain.Guess(pt.X, pt.Y);
                if (guess == 1)
                {
                    CommonFunctions.FillEllipse(pt.X + 18, pt.Y + 18, 10, 10, Brushes.DeepPink, g);
                }
                else
                {
                    CommonFunctions.FillEllipse(pt.X + 18, pt.Y + 18, 10, 10, Brushes.BlueViolet, g);
                }
            }
        }
    }
}
